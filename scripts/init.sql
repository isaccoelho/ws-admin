create database db_hotel;

\c db_hotel

create table guests (
 id         serial not null ,
 company_id  integer         ,         
 room_id     integer          ,        
 docnumber   varchar(50)    ,
 name        varchar(250)   ,
 email       varchar(250)   ,
 mobile      varchar(20)    ,
 password    varchar(1000)  
);


create table users (
   id serial not null,   
   data timestamp not null default current_timestamp,
   name   varchar(250) 
   email varchar(250) ,
   secret varchar(100) ,
    passwd varchar(1000) ,
   token  varchar(1000) ,
   status varchar(10)
);


create table messages (
   id serial not null, 
   company_id integer, 
   room_id integer, 
   guest_id integer, 
   message varchar(2000), 
   dtrequest timestamp not null default current_timestamp, 
   status varchar(10) default 'ABERTO'
); 



create table rooms (
   id serial not null,
   company_id integer,
   number varchar(20),
   location varchar(250),
   comment varchar(1500),
   status varchar(10)
); 
 

create table company (
   id serial not null,
   cnpj varchar(100),
   name varchar(250),
   status varchar(10)
); 