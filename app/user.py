import psycopg2
import hashlib
import uuid

DSN = 'dbname=db_hotel user=postgres host=db'
INSERT  = 'INSERT INTO users (email, passwd, secret) VALUES (%s, %s, %s)'
VALIDATE= 'SELECT email, passwd, id FROM users WHERE email = %s'
CHECK_TOKEN  = 'SELECT id FROM users WHERE token = %s'
UPDATE_TOKEN = 'UPDATE users SET token = %s WHERE id = %s'

GET_COMPANY  = 'SELECT secret FROM users WHERE token = %s'

def add_user(email, passwd):
    conn = psycopg2.connect(DSN)
    cur  = conn.cursor()
    cur.execute(INSERT, (email,passwd,10033,))
    conn.commit()
    cur.close()
    conn.close()

def logout(token):
    conn = psycopg2.connect(DSN)
    cur = conn.cursor()
    tk = token
    record = cur.execute(CHECK_TOKEN, (tk,) )
    record = cur.fetchone()

    if record is not None:
        tk = '' 
        id    = record[0]
        cur.execute(UPDATE_TOKEN, (tk, id,) )
        conn.commit()

    cur.close()
    conn.close()
    return True


def validate_email(email,pwd):
    conn = psycopg2.connect(DSN)
    cur = conn.cursor()
    cur.execute(VALIDATE, (email,) )
    record = cur.fetchone()

    token = 'NOK'
    id = '' 
    pwdb = 'x:x'
 
    if record is not None:
        id    = record[2] 
        pwdb = record[1]
 
    salt = pwdb.split(':') 

    pwdinput =  hashlib.sha512(salt[1].encode() + pwd.encode()).hexdigest() + ':' + salt[1]

    if pwdinput == pwdb:
        salt = uuid.uuid4().hex
        token =  hashlib.sha512(salt.encode() + salt.encode()).hexdigest() 
        cur.execute(UPDATE_TOKEN, (token, id,) )
        conn.commit()

    cur.close()
    conn.close()
    return token

def check_token(tk):
    RESP = 'N'
    conn = psycopg2.connect(DSN)
    cur = conn.cursor()
    cur.execute(CHECK_TOKEN, (tk,) )
    record = cur.fetchone()

    if record == None:
       RESP = 'N'
    else: 
       RESP = 'Y'

    cur.close()
    conn.close()

    return RESP 


def get_company(tk):
    ID = 0
    conn = psycopg2.connect(DSN)
    cur = conn.cursor()
    cur.execute(GET_COMPANY, (tk,) )
    record = cur.fetchone()

    if record == None:
       ID = 0
    else: 
       ID = record[0]

    cur.close()
    conn.close()

    return ID 

