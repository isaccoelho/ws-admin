import psycopg2
from psycopg2.extras import RealDictCursor

DSN = 'dbname=db_hotel user=postgres host=db'
ADD_GUEST = 'INSERT INTO guests (company_id, room_id, docnumber, name, email, mobile, password ) VALUES (%s, %s, %s, %s, %s, %s, %s) ' 
GET_GUESTS = 'SELECT g.id, g.docnumber, g.name, g.email, g.mobile , r.number, r.location FROM guests g, rooms r WHERE g.room_id = r.id AND g.company_id = %s '

def add_guest(record):
    conn = psycopg2.connect(DSN)
    cur  = conn.cursor()

    c1 = record.get('company_id') 
    c2 = record.get('room_id') 
    c3 = record.get('docnumber')
    c4 = record.get('name')
    c5 = record.get('email')
    c6 = record.get('mobile') 
    c7 = record.get('password')

    cur.execute(ADD_GUEST, (c1,c2,c3,c4,c5,c6,c7,))
    conn.commit()
    cur.close()
    conn.close()


def get_guests(id):

    conn = psycopg2.connect(DSN)
    cur = conn.cursor(cursor_factory=RealDictCursor)
    cur.execute(GET_GUESTS, (id, ))
    room_records = cur.fetchall()
    cur.close()
    conn.close()
    return room_records 

