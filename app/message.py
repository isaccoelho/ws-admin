import psycopg2
from psycopg2.extras import RealDictCursor

DSN = 'dbname=db_hotel user=postgres host=db'
ADD_MSG = 'INSERT INTO MESSAGES (company_id, room_id, guest_id, message) VALUES (%s, %s, %s, %s)'
GET_MSG = "SELECT m.message , TO_CHAR(m.dtrequest, 'dd/mm/yyyy hh24:mi:ss') AS dtrequest , r.number, g.name FROM messages m, rooms r, guests g WHERE m.room_id = r.id AND m.guest_id = g.id AND m.company_id = %s ORDER BY m.dtrequest DESC" 

def add_message(record):
    conn = psycopg2.connect(DSN)
    cur  = conn.cursor()

    c1 = record.get('company_id') 
    c2 = record.get('room_id') 
    c3 = record.get('guest_id') 
    c4 = record.get('message')

    cur.execute(ADD_MSG, (c1,c2,c3,c4,))
    conn.commit()
    cur.close()
    conn.close()


def get_messages(id):

    conn = psycopg2.connect(DSN)
    cur = conn.cursor(cursor_factory=RealDictCursor)
    cur.execute(GET_MSG, (id, ))
    room_records = cur.fetchall()
    cur.close()
    conn.close()
    return room_records 

