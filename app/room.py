import psycopg2
from psycopg2.extras import RealDictCursor

DSN = 'dbname=db_hotel user=postgres host=db'
ADD_ROOM = 'INSERT INTO rooms (company_id, number, location, comment, status) VALUES (%s, %s, %s, %s, %s)'
GET_ROOMS = 'SELECT id, company_id, number, location, comment, status FROM rooms WHERE company_id = %s '

 
def add_room(record):
    conn = psycopg2.connect(DSN)
    cur  = conn.cursor()

    c1 = record.get('company_id')
    c2 = record.get('number')
    c3 = record.get('location')
    c4 = record.get('comment')
    c5 = record.get('status')

    cur.execute(ADD_ROOM, (c1,c2,c3,c4,c5,))
    conn.commit()
    cur.close()
    conn.close()


def get_rooms(id):

    conn = psycopg2.connect(DSN)
    cur = conn.cursor(cursor_factory=RealDictCursor)
    cur.execute(GET_ROOMS, (id, ))
    room_records = cur.fetchall()
    cur.close()
    conn.close()
    return room_records 

