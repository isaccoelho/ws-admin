import json
from bottle import Bottle, route, run, request, hook, abort , response, template
from truckpad.bottle.cors import CorsPlugin, enable_cors
import user
import room
import guest
import message


def allow_cors(func):
    """ this is a decorator which enable CORS for specified endpoint """
    def wrapper(*args, **kwargs):
        # set CORS headers
        response.headers['Access-Control-Allow-Origin'] = '*'
        response.headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, OPTIONS'
        response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'

        return func(*args, **kwargs)

    return wrapper

app = Bottle()

@hook('before_request')
def validate_token():
    method =  request.fullpath
    yes = "/logout" in method
    if not yes:
        yes = "/validate" in method
        if not yes:
            yes = "/healthy" in method
 
    if yes  or method == "/":
        pass
    else:
        body = request.body.read().decode('utf8') # read directly HTTP input
        record = json.loads(body) 
        tk = record.get('token')
        us = user.check_token(tk)
        if us == 'N':
            abort(401, "Invalid Token")
    return None

@route('/login', method='POST')
def login():
    email = "isac.coelho@gmail.com"
    senha = "2f464b0b4fb837df4bdb761f480ecf6ca46fd1f8cb6669a4a75d0332db0faac067f1f79fe8f2e44e965bae541afbcffabd700872c82bda4bb114af52890dfdce:78dd594ffe4e4ea9bf9896d8b5d6db85"
    user.add_user(email,senha)
    return 'email recebido {}'.format(
         email
    )


@route('/healthy', method='GET')
@allow_cors
def healthy():
    return { "status": "Up"}

@route('/', method='GET')
@allow_cors
def index():
    return template('web/index.html', messages=[])

@route('/logout/<token>' , method='GET')
@allow_cors
def logout(token):
    user.logout(token)
    return {'token': ''}


@route('/validate', method='POST')
@allow_cors
def validate():
    username = request.forms.get('username')
    password = request.forms.get('password')
    token = user.validate_email(username,password)
    return {'token': token}

@route('/room', method='POST')
def addroom():
    record = load_request()
    room.add_room(record)
    return { "code": "0"}


@route('/rooms', method='POST')
def getrooms():
    record = load_request()
    COMPANY_ID = record.get('company_id')
    app_json = json.dumps(room.get_rooms(COMPANY_ID), sort_keys=True)
    return app_json


@route('/guest', method='POST')
def addguest():
    record = load_request()
    guest.add_guest(record)
    return { "code": "0"}


@route('/guests', method='POST')
def getguests():
    record = load_request()
    COMPANY_ID = record.get('company_id')
    app_json = json.dumps(guest.get_guests(COMPANY_ID), sort_keys=True)
    return app_json


@route('/message', method='POST')
def addmessage():
    record = load_request()
    message.add_message(record)
    return { "code": "0"}


@route('/messages', method='POST')
def getmessages():
    record = load_request()
    COMPANY_ID = record.get('company_id')
    app_json = json.dumps(message.get_messages(COMPANY_ID), sort_keys=True)
    return app_json


def load_request():
    body = request.body.read().decode('utf8') # read directly HTTP input
    record  = json.loads(body) 
    tk = record.get('token')
    record['company_id'] = user.get_company(tk) 
    return record


if __name__ == '__main__':
    run(host='0.0.0.0', port=8080, debug=True)

 
